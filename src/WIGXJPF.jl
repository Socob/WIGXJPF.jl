module WIGXJPF

# TODO: use Ccalls for wig*jj
#using Ccalls
using Wigxjpf_jll

const HalfInteger = Union{Integer, Rational}
const WIGXJPF_LIB = wigxjpf


# “simplified” WIGXJPF interface

export wig_init, wig_free,
	wig_table_init, wig_table_free,
	wig_temp_init, wig_thread_temp_init, wig_temp_free,
	wig3j, wig6j, wig9j


function wig_init(max_j::HalfInteger, wigner_type::Integer)
	wig_table_init(max_j, wigner_type)
	wig_temp_init(max_j)
	return
end

function wig_free()
	wig_temp_free()
	wig_table_free()
	return
end

# void wig_table_init(int max_two_j, int wigner_type);
function wig_table_init(max_j::HalfInteger, wigner_type::Integer)
	max_j ≥ 0 && denominator(max_j) in (1, 2) ||
		error("max_j must be (positive) integer or half-integer")
	wigner_type in (3, 6, 9) || error("wigner_type must be one of (3, 6, 9")
	return @ccall WIGXJPF_LIB.wig_table_init(
		(2max_j)::Cint, wigner_type::Cint)::Cvoid
end

# void wig_table_free(void);
function wig_table_free()
	return @ccall WIGXJPF_LIB.wig_table_free()::Cvoid
end

# void wig_temp_init(int max_two_j);
function wig_temp_init(max_j::HalfInteger)
	max_j ≥ 0 && denominator(max_j) in (1, 2) ||
		error("max_j must be (positive) integer or half-integer")
	return @ccall WIGXJPF_LIB.wig_temp_init((2max_j)::Cint)::Cvoid
end

# void wig_thread_temp_init(int max_two_j);
function wig_thread_temp_init(max_j::HalfInteger)
	max_j ≥ 0 && denominator(max_j) in (1, 2) ||
		error("max_j must be (positive) integer or half-integer")
	return @ccall WIGXJPF_LIB.wig_thread_temp_init((2max_j)::Cint)::Cvoid
end

# void wig_temp_free(void);
function wig_temp_free()
	return @ccall WIGXJPF_LIB.wig_temp_free()::Cvoid
end

#double wig3jj(int two_j1, int two_j2, int two_j3,
#	      int two_m1, int two_m2, int two_m3);
function wig3j(args::NTuple{6, HalfInteger})::Float64
	for x in args
		denominator(x) in (1, 2) ||
			error("arguments must be integer or half-integer")
	end
	two_j1, two_j2, two_j3, two_m1, two_m2, two_m3 = (2x for x in args)
	return @ccall WIGXJPF_LIB.wig3jj(
		two_j1::Cint, two_j2::Cint, two_j3::Cint,
		two_m1::Cint, two_m2::Cint, two_m3::Cint)::Cdouble
end

wig3j(j1::HalfInteger, j2::HalfInteger, j3::HalfInteger,
		m1::HalfInteger, m2::HalfInteger, m3::HalfInteger)::Float64 =
	wig3j((j1, j2, j3, m1, m2, m3))

#double wig6jj(int two_j1, int two_j2, int two_j3,
#	      int two_j4, int two_j5, int two_j6);
function wig6j(args::NTuple{6, HalfInteger})::Float64
	for x in args
		denominator(x) in (1, 2) ||
			error("arguments must be integer or half-integer")
	end
	two_j1, two_j2, two_j3, two_j4, two_j5, two_j6 = (2x for x in args)
	return @ccall WIGXJPF_LIB.wig6jj(
		two_j1::Cint, two_j2::Cint, two_j3::Cint,
		two_j4::Cint, two_j5::Cint, two_j6::Cint)::Cdouble
end

wig6j(j1::HalfInteger, j2::HalfInteger, j3::HalfInteger,
		j4::HalfInteger, j5::HalfInteger, j6::HalfInteger)::Float64 =
	wig6j((j1, j2, j3, j4, j5, j6))


#double wig9jj(int two_j1, int two_j2, int two_j3,
#	      int two_j4, int two_j5, int two_j6,
#	      int two_j7, int two_j8, int two_j9);
function wig9j(args::NTuple{9, HalfInteger})::Float64
	for x in args
		denominator(x) in (1, 2) ||
			error("arguments must be integer or half-integer")
	end
	two_j1, two_j2, two_j3, two_j4, two_j5, two_j6, two_j7, two_j8, two_j9 =
		(2x for x in args)
	return @ccall WIGXJPF_LIB.wig9jj(
		two_j1::Cint, two_j2::Cint, two_j3::Cint,
		two_j4::Cint, two_j5::Cint, two_j6::Cint,
		two_j7::Cint, two_j8::Cint, two_j9::Cint)::Cdouble
end

wig9j(j1::HalfInteger, j2::HalfInteger, j3::HalfInteger,
		j4::HalfInteger, j5::HalfInteger, j6::HalfInteger,
		j7::HalfInteger, j8::HalfInteger, j9::HalfInteger)::Float64 =
	wig9j((j1, j2, j3, j4, j5, j6, j7, j8, j9))


# “normal” WIGXJPF interface

#/* Only call this once, used by all threads.  Returns allocated size,
# * useful for statistics purposes. */
#size_t wigxjpf_fill_factors(int max_factorial); /* 0 to free memory */

#struct wigxjpf_temp;

#/* One temporary array is needed per thread (when used simultaneously). */
#struct wigxjpf_temp *wigxjpf_temp_alloc(int max_iter);

#void wigxjpf_temp_free(struct wigxjpf_temp *temp);

#size_t wigxjpf_temp_size(struct wigxjpf_temp *temp);

#int trivial_zero_3j(int two_j1, int two_j2, int two_j3,
#		    int two_m1, int two_m2, int two_m3);
#int trivial_zero_6j(int two_j1, int two_j2, int two_j3,
#		    int two_j4, int two_j5, int two_j6);
#int trivial_zero_9j(int two_j1, int two_j2, int two_j3,
#		    int two_j4, int two_j5, int two_j6,
#		    int two_j7, int two_j8, int two_j9);

#void calc_3j_double(double *result,
#		    int two_j1, int two_j2, int two_j3,
#		    int two_m1, int two_m2, int two_m3,
#		    struct wigxjpf_temp *temp);
#void calc_6j_double(double *result,
#		    int two_j1, int two_j2, int two_j3,
#		    int two_j4, int two_j5, int two_j6,
#		    struct wigxjpf_temp *temp);
#void calc_9j_double(double *result,
#		    int two_j1, int two_j2, int two_j3,
#		    int two_j4, int two_j5, int two_j6,
#		    int two_j7, int two_j8, int two_j9,
#		    struct wigxjpf_temp *temp);

#void calc_3j_long_double(long double *result,
#			 int two_j1, int two_j2, int two_j3,
#			 int two_m1, int two_m2, int two_m3,
#			 struct wigxjpf_temp *temp);
#void calc_6j_long_double(long double *result,
#			 int two_j1, int two_j2, int two_j3,
#			 int two_j4, int two_j5, int two_j6,
#			 struct wigxjpf_temp *temp);
#void calc_9j_long_double(long double *result,
#			 int two_j1, int two_j2, int two_j3,
#			 int two_j4, int two_j5, int two_j6,
#			 int two_j7, int two_j8, int two_j9,
#			 struct wigxjpf_temp *temp);


end
